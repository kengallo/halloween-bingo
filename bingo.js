let letters = ['B', 'I', 'N', 'G', 'O'];
let cards;

let init = function(timeData) {
  for (let i = 0; i < 5; i++) {
    $('.pattern').append('<button class="number-btn letter">' + letters[Math.floor(i)] + '</button>');
    $('.numbers').append('<button class="number-btn letter">' + letters[Math.floor(i)] + '</button>');
  }
  const today = new Date(timeData?.datetime);
  let d = new Date();

  d.setMonth(9);
  d.setDate(27);
  d.setFullYear(2023);
  d.setHours(23);
  d.setMinutes(59);
  d.setSeconds(59);
  d.setMilliseconds(59);

  let code = prompt("Please enter the code");

  if (today <= d && code === '$h13ldP3ry42023') {
    $('.load').removeClass('hidden');
    $('.any').removeClass('hidden');
    $('.reset').removeClass('hidden');
    $('.legend').removeClass('hidden');

    for (let i = 0; i < 25; i++) {
      let x = i % 5,
        y = Math.floor(i/5),
        free = x == 2 && y == 2;

      $('.pattern').append('<button id="pattern_' + x + '_' + y + '" class="number-btn' + (free ? ' star active' : '') + '" onclick="markPattern(' + x + ', ' + y + ')">' + (free ? '&#9733;' : '&nbsp;') + '</button>');
    }

    for (let i = 1; i <= 75; i++) {
      let display;
      if (i % 5 === 0) {
        display = (15 * 4) + Math.floor(i / 5);
      } else {
        display = (15 * (i % 5 - 1)) + Math.floor(i / 5) + 1;
      }
      $('.numbers').append('<button id="num_' + display + '" class="number-btn" onclick="markNumber(' + display + ')">' + display + '</button>');
    }
  } else {
    alert('Access has expired!');
  }
}

let loadCards = function(text) {
  try {
    cards = JSON.parse(text);

    cards.forEach((card, idx) => {
      let cardDiv = '';

      for (let i = 0; i < 5; i++) {
        cardDiv += '<button class="number-btn letter">' + letters[Math.floor(i)] + '</button>';
      }

      for (let i = 0; i < 25; i++) {
        let x = i % 5,
          y = Math.floor(i/5),
          num = card[letters[x]][y],
          free = num == 0;

        cardDiv += '<button id="card_' + idx + '_' + x + '_' + y + '" class="number-btn' + (free ? ' star active' : '') + '"">' + (free ? '&#9733;' : num) + '</button>';
      }

      $('.cards').append('<div id="card_' + card.name + '" class="card-div"><div class="title"><span>' + card.name + '<a class="remove hidden" onClick="removeCard(\'' + card.name + '\')">&#215;</a></span></div><div class="card">' + cardDiv + '</div></div>');
    });

    $('.game').removeClass('hidden');
    $('.load').addClass('hidden');

    $('input[type="checkbox"][name="preset"]').on('change', evt => {
      const preset = evt.target.value;

      if ($('#row').prop('checked') || $('#column').prop('checked') || $('#diagonal').prop('checked')) {
        $('.uncommon').addClass('hidden');
      } else {
        $('.uncommon').removeClass('hidden');
      }

      if (preset === 'blackout' && $(evt.target).prop('checked')) {
        $('#row').prop('checked', false);
        $('#column').prop('checked', false);
        $('#diagonal').prop('checked', false);
        $('button[id^="pattern"]').addClass('active');
        cards.forEach((card, idx) => {
          $('#card_' + idx + '_2_2').addClass('active');
        });
        $('.uncommon').removeClass('hidden');
      } else {
        $('button[id^="pattern"]').removeClass('active');
        $('#pattern_2_2').addClass('active');
        cards.forEach((card, idx) => {
          $('#card_' + idx + '_2_2').addClass('active');
        });
        $('#blackout').prop('checked', false);
      }
    });
  } catch (e) {
    alert('Please input the correct format!');
  }
};

// let copyCards = function() {
//   let data = [];
//   $('.bingo-card').each((idx, card) => {
//     const name = $(card).attr('id'),
//       B = [$(card).find('.cell_0').html(), $(card).find('.cell_5').html(), $(card).find('.cell_10').html(), $(card).find('.cell_15').html(), $(card).find('.cell_20').html()],
//       I = [$(card).find('.cell_1').html(), $(card).find('.cell_6').html(), $(card).find('.cell_11').html(), $(card).find('.cell_16').html(), $(card).find('.cell_21').html()],
//       N = [$(card).find('.cell_2').html(), $(card).find('.cell_7').html(), 0, $(card).find('.cell_17').html(), $(card).find('.cell_22').html()],
//       G = [$(card).find('.cell_3').html(), $(card).find('.cell_8').html(), $(card).find('.cell_13').html(), $(card).find('.cell_18').html(), $(card).find('.cell_23').html()],
//       O = [$(card).find('.cell_4').html(), $(card).find('.cell_9').html(), $(card).find('.cell_14').html(), $(card).find('.cell_19').html(), $(card).find('.cell_24').html()];

//     data.push({ name, B, I, N, G, O });
//   });
//   navigator.clipboard.writeText(JSON.stringify(data));
// };

let markPattern = function(x, y) {
  let btn = $('#pattern_' + x + '_' + y);

  if (btn.hasClass('active')) {
    btn.removeClass('active');

    if (x == 2 && y == 2) {
      cards.forEach((card, idx) => {
        $('#card_' + idx + '_2_2').removeClass('active');
      });
    }
  } else {
    btn.addClass('active');

    if (x == 2 && y == 2) {
      cards.forEach((card, idx) => {
        $('#card_' + idx + '_2_2').addClass('active');
      });
    }
  }
};

let markNumber = function(num) {
  let btn = $('#num_' + num),
    mark = false,
    patternActive = $('.pattern .active').length,
    cardActive,
    anyRow = $('#row').is(':checked'),
    anyColumn = $('#column').is(':checked'),
    anyDiagonal = $('#diagonal').is(':checked');

  if (btn.hasClass('active')) {
    btn.removeClass('active');
  } else {
    btn.addClass('active');
    mark = true;
  }

  cards.forEach((card, idx) => {
    for (let i = 0; i < 25; i++) {
      let x = i % 5,
        y = Math.floor(i/5),
        btn = $('#card_' + idx + '_' + x + '_' + y);

      if (btn.html() == num) {
        if (mark) {
          if ($('#pattern_' + x + '_' + y).hasClass('active')) {
            btn.addClass('active');
          } else {
            btn.addClass('marked');
          }
        } else {
          btn.removeClass('active').removeClass('marked');
        }
      }
    }

    if (!anyRow && !anyColumn && !anyDiagonal && patternActive > 3) {
      cardActive = $('#card_' + card.name + ' .active').length;
      if ((patternActive - cardActive) < 2) {
        $('#card_' + card.name).attr('class', 'card-div last-1');
      } else if ((patternActive - cardActive) < 3) {
        $('#card_' + card.name).attr('class', 'card-div last-2');
      } else if ((patternActive - cardActive) < 4) {
        $('#card_' + card.name).attr('class', 'card-div last-3');
      } else {
        $('#card_' + card.name).attr('class', 'card-div');
      }

      $('#card_' + card.name).find('.remove').addClass('hidden');
      if (patternActive == cardActive) {
        $('#card_' + card.name).attr('class', 'card-div bingo');
        $('#card_' + card.name).find('.remove').removeClass('hidden');
        alert('BINGO! ' + card.name);
      }
    }
  });

  if (anyRow || anyColumn || anyDiagonal) {
    cards.forEach((card, idx) => {
      let maxMarked = 0;

      if (anyRow) {
        for (let i = 0; i < 5; i++) {
          let ctr = 0,
            cell1 = $('#card_' + idx + '_0_' + i),
            cell2 = $('#card_' + idx + '_1_' + i),
            cell3 = $('#card_' + idx + '_2_' + i),
            cell4 = $('#card_' + idx + '_3_' + i),
            cell5 = $('#card_' + idx + '_4_' + i);

          if (cell1.is('.marked, .active')) ctr++;
          if (cell2.is('.marked, .active')) ctr++;
          if (cell3.is('.marked, .active')) ctr++;
          if (cell4.is('.marked, .active')) ctr++;
          if (cell5.is('.marked, .active')) ctr++;

          if (ctr >= 3 && ctr > maxMarked) {
            maxMarked = ctr;
            if (ctr === 5) {
              cell1.removeClass('marked').addClass('active');
              cell2.removeClass('marked').addClass('active');
              cell3.removeClass('marked').addClass('active');
              cell4.removeClass('marked').addClass('active');
              cell5.removeClass('marked').addClass('active');
            } else {
              $('#card_' + card.name).find('.active').not('.star').addClass('marked').removeClass('active');
            }
          }
        }
      }

      if (anyColumn) {
        for (let i = 0; i < 5; i++) {
          let ctr = 0,
            cell1 = $('#card_' + idx + '_' + i + '_0'),
            cell2 = $('#card_' + idx + '_' + i + '_1'),
            cell3 = $('#card_' + idx + '_' + i + '_2'),
            cell4 = $('#card_' + idx + '_' + i + '_3'),
            cell5 = $('#card_' + idx + '_' + i + '_4');

          if (cell1.is('.marked, .active')) ctr++;
          if (cell2.is('.marked, .active')) ctr++;
          if (cell3.is('.marked, .active')) ctr++;
          if (cell4.is('.marked, .active')) ctr++;
          if (cell5.is('.marked, .active')) ctr++;

          if (ctr >= 3 && ctr > maxMarked) {
            maxMarked = ctr;
            if (ctr === 5) {
              cell1.removeClass('marked').addClass('active');
              cell2.removeClass('marked').addClass('active');
              cell3.removeClass('marked').addClass('active');
              cell4.removeClass('marked').addClass('active');
              cell5.removeClass('marked').addClass('active');
            } else {
              $('#card_' + card.name).find('.active').not('.star').addClass('marked').removeClass('active');
            }
          }
        }
      }

      if (anyDiagonal) {
        let slashCtr = 1,
          slash1 = $('#card_' + idx + '_4_0'),
          slash2 = $('#card_' + idx + '_3_1'),
          slash3 = $('#card_' + idx + '_1_3'),
          slash4 = $('#card_' + idx + '_0_4'),
          backslashCtr = 1,
          backslash1 = $('#card_' + idx + '_0_0'),
          backslash2 = $('#card_' + idx + '_1_1'),
          backslash3 = $('#card_' + idx + '_3_3'),
          backslash4 = $('#card_' + idx + '_4_4');

        if (slash1.is('.marked, .active')) slashCtr++;
        if (slash2.is('.marked, .active')) slashCtr++;
        if (slash3.is('.marked, .active')) slashCtr++;
        if (slash4.is('.marked, .active')) slashCtr++;

        if (backslash1.is('.marked, .active')) backslashCtr++;
        if (backslash2.is('.marked, .active')) backslashCtr++;
        if (backslash3.is('.marked, .active')) backslashCtr++;
        if (backslash4.is('.marked, .active')) backslashCtr++;

        let ctr = Math.max(slashCtr, backslashCtr);

        if (ctr >= 3 && ctr > maxMarked) {
          maxMarked = ctr;
          if (slashCtr === 5) {
            slash1.removeClass('marked').addClass('active');
            slash2.removeClass('marked').addClass('active');
            slash3.removeClass('marked').addClass('active');
            slash4.removeClass('marked').addClass('active');
          } else if (backslashCtr === 5) {
            backslash1.removeClass('marked').addClass('active');
            backslash2.removeClass('marked').addClass('active');
            backslash3.removeClass('marked').addClass('active');
            backslash4.removeClass('marked').addClass('active');
          } else {
            $('#card_' + card.name).find('.active').not('.star').addClass('marked').removeClass('active');
          }
        }
      }

      $('#card_' + card.name).find('.remove').addClass('hidden');
      if (maxMarked === 3) {
        $('#card_' + card.name).attr('class', 'last-2');
      } else if (maxMarked === 4) {
        $('#card_' + card.name).attr('class', 'last-1');
      } else if (maxMarked === 5) {
        $('#card_' + card.name).addClass('bingo');
        $('#card_' + card.name).find('.remove').removeClass('hidden');
        alert('BINGO! ' + card.name);
      } else {
        $('#card_' + card.name).attr('class', 'card-div');
      }
    });
  }
};

let removeCard = function(cardName) {
  $('#card_' + cardName).remove();
};

let reset = function() {
  let confirmReset = confirm('Are you sure you want to reset?');
  if (confirmReset) {
    $('#row').prop('checked', false);
    $('#column').prop('checked', false);
    $('#diagonal').prop('checked', false);
    $('#blackout').prop('checked', false);
    $('.uncommon').removeClass('hidden');
    $('.active').not('.star').removeClass('active');
    $('.marked').removeClass('marked');
    $('.last-1').removeClass('last-1');
    $('.last-2').removeClass('last-2');
    $('.last-3').removeClass('last-3');
    $('.bingo').removeClass('bingo');
    $('.remove').addClass('hidden');
  }
};
